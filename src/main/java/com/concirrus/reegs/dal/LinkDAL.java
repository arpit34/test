package com.concirrus.reegs.dal;

import com.concirrus.reegs.model.VerifySpeedLimit.LinkDetail;
import java.util.List;

public interface LinkDAL {

    LinkDetail getLinkDetail(Long linkId);

    LinkDetail addLinkDetail(LinkDetail linkDetail);

    boolean deleteLinkDetail(Long linkId);

    LinkDetail updateLinkDetail(LinkDetail linkDetail);

    boolean isVerifiable(Long linkId);

    List<LinkDetail> getAllUnverifiedLinks();

    boolean updatePortExposure(LinkDetail lnk);
}
