package com.concirrus.reegs.dal;

import com.concirrus.reegs.model.VerifySpeedLimit.LinkDetail;
import com.mongodb.WriteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class LinkDALImpl implements LinkDAL {

    @Autowired
    @Qualifier(value = "linkMongoTemplate")
    private MongoTemplate _linkMongoTemplate;

    private LinkDetail _linkDetail;

    @Override
    public LinkDetail getLinkDetail(Long linkId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("linkId").is(linkId));
        _linkDetail = _linkMongoTemplate.findOne(query, LinkDetail.class);
        return _linkDetail;
    }

    @Override
    public LinkDetail addLinkDetail(LinkDetail linkDetail) {
        _linkDetail = new LinkDetail();
        _linkDetail.setLinkId(linkDetail.getLinkId());
        _linkDetail.setVerifiedSpeedLimit(linkDetail.getVerifiedSpeedLimit());
        _linkDetail.setVerifiedBy(linkDetail.getVerifiedBy());
        _linkDetail.setComments(linkDetail.getComments());
        _linkDetail.setLastUpdatedOn(linkDetail.getLastUpdatedOn());
        _linkDetail.setVerifiable(linkDetail.isVerifiable());
        _linkDetail.setVerifiable(linkDetail.isVerified());
        _linkMongoTemplate.save(_linkDetail);

        return _linkDetail;
    }

    @Override
    public boolean deleteLinkDetail(Long linkId) {

        Query query = new Query();

        query.addCriteria(Criteria.where("linkId").is(linkId));
        WriteResult writeResult = _linkMongoTemplate.remove(query, LinkDetail.class);

        if (writeResult.wasAcknowledged())
            return true;

        else
            return false;
    }

    @Override
    public LinkDetail updateLinkDetail(LinkDetail linkDetail) {

        Query query = new Query();
        Update update = new Update();

        query.addCriteria(Criteria.where("linkId").is(linkDetail.getLinkId()));

        update.set("linkId", linkDetail.getLinkId());
        update.set("verifiedSpeedLimit", linkDetail.getVerifiedSpeedLimit());
        update.set("verifiedBy", linkDetail.getVerifiedBy());
        update.set("comments",linkDetail.getComments());
        update.set("lastUpdatedOn", linkDetail.getLastUpdatedOn());
        update.set("verifiable", linkDetail.isVerifiable());
        update.set("verified",linkDetail.isVerified());
        WriteResult wr = _linkMongoTemplate.updateFirst(query, update, LinkDetail.class);

        if (wr.isUpdateOfExisting()) {
            _linkDetail = getLinkDetail(linkDetail.getLinkId());
            return _linkDetail;
        } else
            return null;

    }

    @Override
    public boolean isVerifiable(Long linkId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("linkId").is(linkId));
        _linkDetail = _linkMongoTemplate.findOne(query, LinkDetail.class);

        if(!_linkDetail.isVerifiable())
            return false;

        else
            return true;
    }

    @Override
    public List<LinkDetail> getAllUnverifiedLinks() {
        return null;
    }

    @Override
    public boolean updatePortExposure(LinkDetail lnk) {
        return false;
    }

}
