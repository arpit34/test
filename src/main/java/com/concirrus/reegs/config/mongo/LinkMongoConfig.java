package com.concirrus.reegs.config.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class LinkMongoConfig {

    @Value("${link.mongodb.host}")
    private String host;
    @Value("${link.mongodb.database}")
    private String database;
    @Value("${link.mongodb.username}")
    private String username;
    @Value("${link.mongodb.password}")
    private String password;
    @Value("${link.mongodb.port}")
    private int port;
    @Value("${link.mongodb.authentication-database}")
    private String authenticationDatabase;

    public MongoDbFactory mongoDbFactory() throws Exception {
        List<MongoCredential> credentialsList = new ArrayList<>();
        MongoCredential creds = MongoCredential.createCredential(username, authenticationDatabase, password.toCharArray());
        credentialsList.add(creds);
        ServerAddress serverAddress = new ServerAddress(host, port);
        return new SimpleMongoDbFactory(new MongoClient(serverAddress), database);
    }

    @Primary
    public @Bean(name = "linkMongoTemplate")
    MongoTemplate getMongoTemplate() throws Exception {
        return new MongoTemplate(mongoDbFactory());
    }
}