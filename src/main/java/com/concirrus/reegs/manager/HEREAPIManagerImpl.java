package com.concirrus.reegs.manager;

import com.concirrus.reegs.model.RoadSpeedRequest.Location;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

@Configuration
@Service
public class HEREAPIManagerImpl implements HEREAPIManager{

    @Value("${here.api.baseurl}")
    private String baseurl;

    @Value("${here.api.appID}")
    private String AppID;

    @Value("${here.api.appCode}")
    private String AppCode;

    @Override
    public String getGPXXML(Location[] latlngPointsFromJourney) {
        //Create GPX from those journeys (Convert multiple lat,lng to GPX XML)
        String header = "<?xml version=\'1.0\'?>";
        String gpxHeader = "<gpx version=\'1.0\' xmlns:xsi=\'http://www.w3.org/2001/XMLSchema-instance\' xmlns=\'http://www.topografix.com/GPX/1/0\' xsi:schemaLocation=\'http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd\'>";
        String trkNode = "<trk>";
        String trkSegNode = "<trkseg>";
        String trkNodeEnd = "</trk>";
        String trk1 = "</trkpt>";
        String trkSegNodeEnd = "</trkseg>";
        String gpxNodeEnd = "</gpx>";
        String xmlForHere = header+gpxHeader+trkNode+trkSegNode;
        for (Location location : latlngPointsFromJourney)
        {
            if(location.getDriverSpeedKPH() > 0.0) {
                String latNode = "<trkpt lat=\"" + location.getLatitude() + "\"" + " lon=\"" + location.getLongitude() + "\">";
                String SpeedNode = "<speed>" + location.getDriverSpeedKPH() + "</speed>";
                String CourseNode = "<course>" + location.getCourse() + "</course>";
                xmlForHere = xmlForHere + latNode + SpeedNode + CourseNode + trk1;
            }
        }
        xmlForHere = xmlForHere+ trkSegNodeEnd + trkNodeEnd + gpxNodeEnd;

        return xmlForHere;
    }

    @Override
    public String getLinkDataFromHere(String gpxXML) throws IOException {
        String url = baseurl + "?routemode=car&attributes=LINK_ATTRIBUTE_FCn(RAMP),SPEED_LIMITS_FCn(FROM_REF_SPEED_LIMIT,TO_REF_SPEED_LIMIT)&app_id=" + AppID + "&app_code=" + AppCode;
        URL obj = new URL(baseurl);
        String hereresponse = sendPOST(url, gpxXML);
        return hereresponse;

    }

    private static String sendPOST(String url,String gpxXML) throws IOException {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);
        OutputStream os = con.getOutputStream();
        os.write(gpxXML.getBytes());
        os.flush();
        os.close();
        int responseCode = con.getResponseCode();
        System.out.println("POST RoadSpeedResponse Code :: " + responseCode);
        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } else {
            return url + " " + responseCode + "Body: " + gpxXML + "POST request not worked";
        }
    }
}



