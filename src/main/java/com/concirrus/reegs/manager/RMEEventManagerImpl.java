package com.concirrus.reegs.manager;

import com.concirrus.reegs.model.RoadSpeedRequest.Journey;
import com.concirrus.reegs.model.RoadSpeedRequest.Location;
import com.concirrus.reegs.model.RoadSpeedResponse.*;
import com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.Attribute;
import com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.Attributes.LINK_ATTRIBUTE_FCN;
import com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.Attributes.SPEED_LIMITS_FCN;
import com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.RouteLink;
import com.concirrus.reegs.model.VerifySpeedLimit.LinkDetail;
import com.concirrus.reegs.model.enums.RMEEventType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

@Configuration
@Service
public class RMEEventManagerImpl implements RMEEventManager {

    private final Logger _logger = LoggerFactory.getLogger(RMEEventManagerImpl.class);

    private HEREAPIManager hereManager;
    private LinkManager linkManager;

    @Autowired
    public RMEEventManagerImpl(HEREAPIManager hereManager, LinkManager linkManager) {
        this.hereManager = hereManager;
        this.linkManager = linkManager;
    }

    /**
     * Service method to GetRMEEvents in Response.
     *
     * @return Return object with appropriate response.
     */
    @Override
    public List<RMEEvent> getRMEEvents(List<Journey> listOfJourney) throws IOException {
        List<RMEEvent> retVal = new ArrayList<RMEEvent>();
        for (Journey journey : listOfJourney) {
            Location[] journeyPoints = journey.getLocation();
            String gpxXML = hereManager.getGPXXML(journeyPoints);
            String hereResponse = hereManager.getLinkDataFromHere(gpxXML);
            if (IsValid(hereResponse)) {
                RMEResponse response = getLocationData(hereResponse);
                Evaluate(retVal, journey, response.getTracePoints(), response.getRouteLinks());
            } else {
                _logger.error("There is no packet received by HERE for this journey. '{}'", hereResponse);
            }
            SetDriverPeakSpeedMph(retVal);
            SetEventdistance(retVal);
            SetEventduration(retVal);
            SetAverageDriverSpeedKPH(retVal);
            SetDateTime(retVal);

        }
        return retVal;
    }

    /**
     * Service method to GetRMEEvents in Response.
     *
     * @return Return object with appropriate response.
     *//*
    @Override
    public List<RMEEvent> getReEvaluateRMEEvents(List<RMEEvent> listOfEvent) throws IOException {
        List<RMEEvent> retVal = new ArrayList<RMEEvent>();
        List<LINK_ATTRIBUTE_FCN> lafcn = new ArrayList<>();
        List<SPEED_LIMITS_FCN> slfcn = new ArrayList<>();
        LINK_ATTRIBUTE_FCN lfc = new LINK_ATTRIBUTE_FCN();
        lfc.setRAMP("N");
        lafcn.add(lfc);
        SPEED_LIMITS_FCN slf = new SPEED_LIMITS_FCN();
        slf.setFROM_REF_SPEED_LIMIT(48.0);
        slf.setTO_REF_SPEED_LIMIT(48.0);
        slfcn.add(slf);
        Attribute att = new Attribute();
        att.setLINK_ATTRIBUTE_FCN(lafcn);
        att.setSPEED_LIMITS_FCN(slfcn);
        Date date = new Date();
        List<RouteLink> lstRT = new ArrayList<RouteLink>();
        for (RMEEvent evt : listOfEvent) {
            for (Links link : evt.getLinks()) {
                RouteLink rlink = new RouteLink();
                rlink.setShape(link.getShape());
                rlink.setLinkId(link.getLinkid());
                rlink.setFunctionalClass(9);
                rlink.setConfidence(6);
                rlink.setLinkLength(67.7);
                rlink.setmSecToReachLinkFromStart(567);
                rlink.setAttributes(att);
                lstRT.add(rlink);
            }
            List<TracePoint> lstTP = new ArrayList<TracePoint>();
                for (Points loc : evt.getPoints()) {
                    TracePoint p = new TracePoint();
                    p.setLat(loc.getLatitude());
                    p.setLon(loc.getLongitude());
                    p.setHeadingDegreeNorthClockwise((int) loc.getCourse());
                    p.setLinkIdMatched(loc.getLinkID());
                    p.setConfidenceValue(67);
                    p.setElevation(56);
                    p.setHeadingMatched(1000);
                    p.setLatMatched(loc.getLatitude());
                    p.setMatchDistance(8.9);
                    p.setMatchOffsetOnLink(9.0);
                    p.setMinError(9);
                    p.setRouteLinkSeqNrMatched(5);
                    p.setSpeedMps(loc.getDriverSpeedKPH());
                    p.setTimestamp(date);
                    lstTP.add(p);
                }
                Journey jrn = new Journey();
                jrn.setIMEI(evt.getIMEI());

                Location[] location = new Location[evt.getPoints().size()];
                int pointIndex = 0;
                for (Points pt : evt.getPoints()) {
                    Location lc = new Location();
                    TracePoint tcp = new TracePoint();
                    lc.setCourse(pt.getCourse());
                    lc.setLatitude(pt.getLatitude());
                    lc.setLongitude(pt.getLongitude());
                    lc.setDriverSpeedKPH(pt.getDriverSpeedKPH());
                    lc.setEventDateTime(pt.getEventDateTime());
                    location[pointIndex] = lc;
                    double d = pt.getCourse();
                    Integer course = (int) d;
                    tcp.setHeadingDegreeNorthClockwise(course);
                    tcp.setLat(pt.getLatitude());
                    tcp.setLon(pt.getLongitude());
                    tcp.setTimestamp(pt.getEventDateTime());
                    lstTP.add(tcp);
                    pointIndex++;
                }
                jrn.setLocation(location);
                Evaluate(retVal, jrn, lstTP, lstRT);
                SetDriverPeakSpeedMph(retVal);
                SetEventdistance(retVal);
                SetEventduration(retVal);
                SetAverageDriverSpeedKPH(retVal);
                SetDateTime(retVal);
        }
                return retVal;
    }*/

    /**
     * Service method to GetRMEEvents in Response.
     *
     * @return Return object with appropriate response.
     */
    @Override
    public List<RMEEvent> getReEvaluateRMEEvents(List<RMEEvent> listOfEvent) throws IOException {
        List<RMEEvent> retVal = new ArrayList<RMEEvent>();
        List<LINK_ATTRIBUTE_FCN> lafcn = new ArrayList<>();
        List<SPEED_LIMITS_FCN> slfcn = new ArrayList<>();
        LINK_ATTRIBUTE_FCN lfc = new LINK_ATTRIBUTE_FCN();
        lfc.setRAMP("N");
        lafcn.add(lfc);
        SPEED_LIMITS_FCN slf = new SPEED_LIMITS_FCN();
        slf.setFROM_REF_SPEED_LIMIT(48.0);
        slf.setTO_REF_SPEED_LIMIT(48.0);
        slfcn.add(slf);
        Attribute att = new Attribute();
        att.setLINK_ATTRIBUTE_FCN(lafcn);
        att.setSPEED_LIMITS_FCN(slfcn);
        Date date = new Date();
        List<RouteLink> lstRT = new ArrayList<RouteLink>();
        for (RMEEvent evt : listOfEvent) {
            for (Links link : evt.getLinks()) {
                RouteLink rlink = new RouteLink();
                rlink.setShape(link.getShape());
                rlink.setLinkId(link.getLinkid());
                rlink.setAttributes(att);
                lstRT.add(rlink);
            }
            List<TracePoint> lstTP = new ArrayList<TracePoint>();
            for (Points loc : evt.getPoints()) {
                TracePoint p = new TracePoint();
                p.setLat(loc.getLatitude());
                p.setLon(loc.getLongitude());
                p.setHeadingDegreeNorthClockwise((int) loc.getCourse());
                p.setLinkIdMatched(loc.getLinkID());
                p.setLatMatched(loc.getLatitude());
                p.setSpeedMps(loc.getDriverSpeedKPH());
                p.setTimestamp(date);
                lstTP.add(p);
            }
            Journey jrn = new Journey();
            jrn.setIMEI(evt.getIMEI());
            Location[] location = new Location[evt.getPoints().size()];
            int pointIndex = 0;
            for (Points pt : evt.getPoints()) {
                Location lc = new Location();
                TracePoint tcp = new TracePoint();
                lc.setCourse(pt.getCourse());
                lc.setLatitude(pt.getLatitude());
                lc.setLongitude(pt.getLongitude());
                lc.setDriverSpeedKPH(pt.getDriverSpeedKPH());
                lc.setEventDateTime(pt.getEventDateTime());
                location[pointIndex] = lc;
                double d = pt.getCourse();
                Integer course = (int) d;
                tcp.setHeadingDegreeNorthClockwise(course);
                tcp.setLat(pt.getLatitude());
                tcp.setLon(pt.getLongitude());
                tcp.setTimestamp(pt.getEventDateTime());
                lstTP.add(tcp);
                pointIndex++;
            }
            jrn.setLocation(location);
            Evaluate(retVal, jrn, lstTP, lstRT);
            SetDriverPeakSpeedMph(retVal);
            SetEventdistance(retVal);
            SetEventduration(retVal);
            SetAverageDriverSpeedKPH(retVal);
            SetDateTime(retVal);
        }
        return retVal;
    }

    /**
     * Service method to LookupForLinkId in Response.
     * @return Return object with appropriate response.
     */
    private void LookupForLinkId(TracePoint tp, RMEEvent event,List<Points> lstPoint,Journey journey) {
        Long lnkId = tp.getLinkIdMatched();
        LinkDetail lnk = linkManager.SearchVerifiedLinkId(lnkId);
        if (lnk != null)
        {
            //Case: When link found in Collection and marked verified also
            event.setSpeedLimit(lnk.getVerifiedSpeedLimit());
            event.setVerified(true);
            event.setverifiable(false);
        }
        else if (linkManager.SearchLinkId(lnkId) != null)
        {
            lnk = linkManager.SearchLinkId(lnkId);
            //Case: When link found in Collection but not marked verified
            if(lnk.isVerifiable()) {
                event.setverifiable(true);
            }else {
                event.setverifiable(false);
                event.setVerified(false);
            }
        }
        else {
            //Case: link not found in Collection then update HERE speedlimit and mark it for review
            LinkDetail link = new LinkDetail();
            Points p = new Points();
            event.setVerified(false);
            event.setverifiable(false);
            LinkDetail ld = new LinkDetail();
            ld.setLinkId(lnkId);
            ld.setVerifiable(true);
            ld.setVerifiedSpeedLimit(event.getSpeedLimit());
            ld.setVerifiedBy("System");
            ld.setComments("");
            ld.setVerified(false);
            for (Location loc : journey.getLocation()) {
                if (loc.getLatitude() == tp.getLat() && loc.getLongitude() == tp.getLon()) {
                    ld.setLastUpdatedOn(loc.getEventDateTime());
                    break;
                }
            }
            linkManager.addLinkDetail(ld);
        }
    }

    /**
     * Service method to GetEvalute in Response.
     * @return Return object with appropriate response.
     */
    private void Evaluate(List<RMEEvent> retVal, Journey journey, List<TracePoint> lstTP, List<RouteLink> lstRT) {
        RMEEventType prevEventType = RMEEventType.Overspeed;
        Long prevLinkId = 0L;
        Double prevSpeedLimit = 0.0;
        Integer firstCount = 0;
        Integer index = 1;
        List<Links> listOfLink = new ArrayList<>();
        List<Points> listOfPoints = new ArrayList<>();
        List<Links> prevListOfLink = new ArrayList<>();
        List<Points> prevListOfPoints = new ArrayList<>();
        for (TracePoint tp : lstTP){
            RMEEvent event = new RMEEvent();
            SetJourneyDetailstoEvent(journey, event);
            SetGPSDatatoEvent(tp, event);
            SetLinkIdtoEvent(tp, event);
            SetRoadLimitAtPeakKPH(retVal);
            LookupForLinkId(tp, event,listOfPoints,journey);
            SetSpeedlimit(event,lstRT);
            _logger.info("Current Event ID {}", event.getLinkId());
            _logger.info("Current Event Index {}", index);
            if (event.getSpeedLimit() == 0.0)
            {
                int count = retVal.size();
                if(count > 1) {
                    retVal.get(count - 1).setPoints(prevListOfPoints);
                    retVal.get(count - 1).setLinks(prevListOfLink);
                }
                index++;
                prevEventType = RMEEventType.None;
                prevSpeedLimit = 0.0;
                prevLinkId = event.getLinkId();

                _logger.info("Event Ignored due to Speed Limit");
                continue;
            }
            SetEventType(event,listOfPoints,listOfLink,tp);
            if (event.getEventType() == RMEEventType.Extremeoverspeed) {
                event.setIsExtreme(true);
            } else if (event.getEventType() == RMEEventType.Overspeed) {
                event.setIsExtreme(false);
            } else{
                prevEventType = event.getEventType();
            }

            SetRampStatus(event,tp,lstRT);
            _logger.info("Current Event {}", event.getEventType());
            if(event.getEventType() == null || event.getEventType() == RMEEventType.None ) {
                int count = retVal.size();
                if(count > 1) {
                    retVal.get(count - 1).setPoints(prevListOfPoints);
                    retVal.get(count - 1).setLinks(prevListOfLink);
                }
                index++;
                prevEventType = event.getEventType();
                prevLinkId = event.getLinkId();
                prevSpeedLimit = 0.0;
                _logger.info("Event Ignored due to Event Type");
                continue;
            }
            if (firstCount > 0) {
                if (prevEventType != event.getEventType() || (!prevSpeedLimit.equals(event.getSpeedLimit()))) {
                    int count = retVal.size();
                    retVal.get(count - 1).setPoints(listOfPoints);
                    retVal.get(count - 1).setLinks(listOfLink);
                    listOfLink = new ArrayList<>();
                    listOfPoints = new ArrayList<>();
                    if (!retVal.contains(event)) {
                        _logger.info("Adding new Event");
                        retVal.add(event);
                    }
                    SetPoints(event, tp, listOfPoints, journey);
                    SetLink(event, tp, listOfLink, lstRT);
                    prevListOfLink = listOfLink;
                    prevListOfPoints = listOfPoints;
                    if (lstTP.size() == index) {
                        event.setLinks(listOfLink);
                        event.setPoints(listOfPoints);
                    }
                } else if (lstTP.size() == index) {
                    int count = retVal.size();
                    if (!prevLinkId.equals(event.getLinkId())) {
                        SetLink(event, tp, listOfLink, lstRT);
                        prevListOfLink = listOfLink;
                    }
                    SetPoints(event, tp, listOfPoints, journey);
                    prevListOfPoints = listOfPoints;
                    retVal.get(count - 1).setPoints(listOfPoints);
                    retVal.get(count - 1).setLinks(listOfLink);
                    listOfLink = new ArrayList<>();
                    listOfPoints = new ArrayList<>();
                } else {
                    if (!prevLinkId.equals(event.getLinkId())) {
                        SetLink(event, tp, listOfLink, lstRT);
                        prevListOfLink = listOfLink;
                    }
                    SetPoints(event, tp, listOfPoints, journey);
                    prevListOfPoints = listOfPoints;
                }
            } else {
                _logger.info("First Event in a journey");
                if (!event.isIgnored()) {
                    _logger.info("Adding new Event");
                    retVal.add(event);
                    SetLink(event, tp, listOfLink,lstRT);
                    SetPoints(event, tp, listOfPoints, journey);
                    prevListOfLink = listOfLink;
                    prevListOfPoints = listOfPoints;
                    if (lstTP.size() == index){
                        int count = retVal.size();
                        retVal.get(count - 1).setPoints(listOfPoints);
                        retVal.get(count -1).setLinks(listOfLink);
                        listOfPoints = new ArrayList<>();
                        listOfLink = new ArrayList<>();
                    }
                    firstCount++;
                }
            }
            _logger.info("PrevEventType {}", prevEventType);
            _logger.info("CurrEventType {}", event.getEventType());
            prevEventType = event.getEventType();
            _logger.info("PrevLinkID {}", prevLinkId);
            _logger.info("CurrLinkID {}", event.getLinkId());
            prevLinkId = event.getLinkId();
            _logger.info("PrevSpeedLimit {}", prevSpeedLimit);
            _logger.info("CurrSpeedLimit {}", event.getSpeedLimit());
            prevSpeedLimit = event.getSpeedLimit();
            index++;
        }
    }

    /**
     * Service method to GetDriverPeakSpeed in Response.
     * @return Return object with appropriate response.
     */
    private void SetDriverPeakSpeedMph(List<RMEEvent> retVal) {
        for (RMEEvent evt : retVal) {
            Double peakSpeed = 0.0;
            if(evt.getPoints() == null){
                continue;
            }
            for (Points pt : evt.getPoints()) {
                if (peakSpeed < pt.getDriverSpeedKPH()){
                    peakSpeed = pt.getDriverSpeedKPH();
                }
            }
            evt.setDriverSpeedAtPeakKPH(peakSpeed);
        }
    }

    /**
     * Service method to SetDaTETime in Response.
     * @return Return object with appropriate response.
     */
    private void SetDateTime(List<RMEEvent> retVal) {
        List<Points> eventpoints = new ArrayList<Points>();
        for (RMEEvent event : retVal) {
            eventpoints = event.getPoints();
            if (eventpoints == null || eventpoints.isEmpty()) {
                continue;
            }
                event.setDatetime(eventpoints.get(0).getEventDateTime());

            }
        }

    /**
     * Service method to GetRoadLimitAtPeak in Response.
     * @return Return object with appropriate response.
     */
    private void SetRoadLimitAtPeakKPH(List<RMEEvent> retVal){
        for (RMEEvent evt : retVal) {
            Double roadspeed = 0.0;
            if(evt.getPoints() == null){
                continue;
            }
            for (Points pt : evt.getPoints()) {
                if (roadspeed < pt.getSpeedLimitKPH()){
                    roadspeed = pt.getSpeedLimitKPH();
                }
            }
            evt.setRoadLimitAtPeakKPH(roadspeed);
        }
    }

    /**
     * Service method to GetEventDuration in Response.
     * @return Return object with appropriate response.
     */
    private void SetEventduration(List<RMEEvent> retVal) {
        for (RMEEvent evt : retVal) {
            long duration1 = 0;
            long diffSeconds = 0;
            Integer index = 1;
            Date firstPtDate = null;
            Date lastPtDate = null;
            if(evt.getPoints() == null){
                continue;
            }
            for (Points pt : evt.getPoints()) {
                if (index == 1) {
                    firstPtDate = (pt.getEventDateTime());

                } else if (evt.getPoints().size() == index) {
                    lastPtDate = (pt.getEventDateTime());
                }
                index++;
            }
            //Calculate duration for seconds
            if(evt.getPoints().size() > 1) {
                duration1 = lastPtDate.getTime() - firstPtDate.getTime();
                diffSeconds = duration1/1000;

            } else{
                diffSeconds = 0;
            }
            evt.setEventDurationSeconds(diffSeconds);
        }
    }

    /**
     * Service method to GetAverageDriverSpeed in Response.
     * @return Return object with appropriate response.
     */
    private void SetAverageDriverSpeedKPH(List<RMEEvent> retVal) {
        for (RMEEvent evt : retVal) {
            double avgSpeed = 0.0;
            if (evt.getPoints() == null) {
                continue;
            }
            Integer index = 0;
            for (Points pg : evt.getPoints()) {
                avgSpeed = avgSpeed + pg.getDriverSpeedKPH();
                index++;
            }
            if(index > 0){
                avgSpeed = avgSpeed/index;
            }
            evt.setEventAverageDriverSpeedKPH(avgSpeed);
        }
    }

    /**
     * Service method to GetEventDistance in Response.
     * @return Return object with appropriate response.
     */
    private void SetEventdistance(List<RMEEvent> retVal) {
        for (RMEEvent evt : retVal) {
            double distance = 0.0;
            Integer index = 1;
            double firstPtLat = 0.0;
            double firstPtLon = 0.0;
            double lastPtLat = 0.0;
            double lastPtLon = 0.0;
            if(evt.getPoints() == null){
                continue;
            }
            for (Points pt : evt.getPoints()) {
                if (index == 1) {
                    firstPtLat = pt.getLatitude();
                    firstPtLon = pt.getLongitude();
                } else if (evt.getPoints().size() == index) {
                    lastPtLat = pt.getLatitude();
                    lastPtLon = pt.getLongitude();
                }
                index++;
            }
            //Calculate distance for meter
            if(evt.getPoints().size() > 1) {
                distance = CalculateDistance(firstPtLat, lastPtLat, firstPtLon, lastPtLon);

            }else{
                distance = 0.0;
            }
            evt.setEventDistanceMetre(Math.round(distance * 100 / 100));

        }
    }

    /**
     * Service method formula to calculate EventDistance.
     * @return Return object with appropriate response.
     */
    private double CalculateDistance(double lat1, double lat2, double lon1, double lon2) {

        final int R = 6371; //Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; //convert to meters
        double height = 0.0;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);
        return Math.sqrt(distance);
    }

    /**
     * Service method to GetGPSDatatoEvent.
     * @return Return object with appropriate response.
     */
    private void SetGPSDatatoEvent(TracePoint tp, RMEEvent event) {
        event.setLat(tp.getLat());
        event.setLon(tp.getLon());
    }

    /**
     * Service method to SetLinkIdToEvent in Response.
     * @return Return object with appropriate response.
     */
    private void SetLinkIdtoEvent(TracePoint tp, RMEEvent event) {
        event.setLinkId(tp.getLinkIdMatched());
    }

    /**
     * Service method to SetJourneyDetailstoEvent in Response.
     * @return Return object with appropriate response.
     */
    private void SetJourneyDetailstoEvent(Journey journey, RMEEvent event) {
        event.setIMEI(journey.getIMEI());
    }

    /**
     * Service method to SetIsvalidHere in Response.
     * @return Return object with appropriate response.
     */
    private boolean IsValid(String hereResponse) {
        if (hereResponse ==null){
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Service method to GetLocationDate from Here in Response.
     * @return Return object with appropriate response.
     */
    private RMEResponse getLocationData(String hereResponse) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(hereResponse, RMEResponse.class);
    }

    /**
     * Service method to GetSpeedLimit from Here in Response.
     * @return Return object with appropriate response.
     */
    private void SetSpeedlimit(RMEEvent evt, List<RouteLink> lstRL){
        if (evt.getSpeedLimit()!=0.0)
        {
            return;
        }
        for (RouteLink rl : lstRL) {
            try {

            if(rl.getLinkId().equals(evt.getLinkId())) {
                if (rl.getAttributes().getSPEED_LIMITS_FCN() != null && !rl.getAttributes().getSPEED_LIMITS_FCN().isEmpty() &&
                        rl.getAttributes().getSPEED_LIMITS_FCN().get(0).getFROM_REF_SPEED_LIMIT() >= rl.getAttributes().getSPEED_LIMITS_FCN().get(0).getTO_REF_SPEED_LIMIT()) {
                    evt.setSpeedLimit(rl.getAttributes().getSPEED_LIMITS_FCN().get(0).getFROM_REF_SPEED_LIMIT());
                }
                else if (rl.getAttributes().getSPEED_LIMITS_FCN() != null && !rl.getAttributes().getSPEED_LIMITS_FCN().isEmpty() &&
                        rl.getAttributes().getSPEED_LIMITS_FCN().get(0).getTO_REF_SPEED_LIMIT() >= rl.getAttributes().getSPEED_LIMITS_FCN().get(0).getFROM_REF_SPEED_LIMIT()) {
                    evt.setSpeedLimit(rl.getAttributes().getSPEED_LIMITS_FCN().get(0).getTO_REF_SPEED_LIMIT());
                }
            }
            }
            catch (Exception ex) {
                _logger.error("Exception occured while fetching Speedlimit. '{}'", ex.getMessage());
            }
        }
    }

    /**
     * Service method to SetEventType in Response.
     * @return Return object with appropriate response.
     */
    private void SetEventType(RMEEvent event,List<Points> listOfPoints,List<Links> listOfLink,TracePoint tp) {
        Points p = new Points();
        int nationalSpeedLimit = 70;
        double extremeThreshold = 24.1402;
        double speedLimit = event.getSpeedLimit();

        if (p.setDriverSpeedKPH(tp.getSpeedMps()) <= speedLimit)
        {
            event.setIgnored(true); //Ignore event
            event.setEventType(RMEEventType.None);
            return;
        }

        if (event.isverifiable())
        {
            //If Link is unverifiable
            if(p.setDriverSpeedKPH(tp.getSpeedMps()) > nationalSpeedLimit){
                if(p.setDriverSpeedKPH(tp.getSpeedMps()) > nationalSpeedLimit+extremeThreshold){
                    event.setEventType(RMEEventType.Extremeoverspeed);
                } else {
                    event.setEventType(RMEEventType.Overspeed);
                }
            }
            else
                {
                event.setIgnored(true); //Ignore event
                event.setEventType(RMEEventType.None);
            }
        }
        else
            {
            if(Math.round(extremeThreshold) <= Math.round(Math.abs(speedLimit- p.setDriverSpeedKPH(tp.getSpeedMps())))){
                event.setEventType(RMEEventType.Extremeoverspeed);
                event.setLinks(listOfLink);
                event.setPoints(listOfPoints);
            }
            else
                {
                event.setEventType(RMEEventType.Overspeed);
                event.setLinks(listOfLink);
                event.setPoints(listOfPoints);
            }
        }
    }

    /**
     * Service method to SetPoints in Response.
     * @return Return object with appropriate response.
     */
    private void SetPoints(RMEEvent event, TracePoint tp, List<Points> lstPoint, Journey journey) {
        Points p = new Points();
        for (Location loc : journey.getLocation()) {
            if (loc.getLatitude() == tp.getLat() && loc.getLongitude() == tp.getLon()) {
                p.setEventDateTime(loc.getEventDateTime());
                break;
            }
        }
        p.setLatitude(tp.getLat());
        p.setLongitude(tp.getLon());
        p.setCourse(tp.getHeadingDegreeNorthClockwise());
        p.setLinkID(tp.getLinkIdMatched());
        p.setDriverSpeedKPH(tp.getSpeedMps());
        p.setSpeedLimitKPH(event.getSpeedLimit());
        lstPoint.add(p);

    }

    /**
     * Service method to SetLinks in Response.
     * @return Return object with appropriate response.
     */
    private void SetLink(RMEEvent event, TracePoint tp, List<Links> lstLink,List<RouteLink> rt){
        Links l = new Links();
        for (RouteLink rl : rt) {
            if(tp.getLinkIdMatched().equals(rl.getLinkId())) {
                l.setShape(rl.getShape());
                break;
            }
        }
        l.setLinkid(tp.getLinkIdMatched());
        l.setSpeedLimitKPH(event.getSpeedLimit());
        l.setSource("HERE");
        lstLink.add(l);

    }

    /**
     * Service method to GetRampStatus in Response.
     * @return Return object with appropriate response.
     */
    private void SetRampStatus(RMEEvent event,TracePoint tp, List<RouteLink> rt) {
        for (RouteLink rl : rt) {
            if (tp.getLinkIdMatched().equals(rl.getLinkId())) {
                if(rl.getAttributes().getLINK_ATTRIBUTE_FCN().get(0).getRAMP().equals("Y")) {
                    event.setIgnored(true);
                }
                break;
            }
        }
    }
}
