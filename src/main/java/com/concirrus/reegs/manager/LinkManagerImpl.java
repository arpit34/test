package com.concirrus.reegs.manager;

import com.concirrus.reegs.dal.LinkDAL;
import com.concirrus.reegs.model.VerifySpeedLimit.LinkDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LinkManagerImpl implements LinkManager {

    private LinkDAL _lnkDAL;

    @Autowired
    public LinkManagerImpl(LinkDAL lnkDAL) {
        this._lnkDAL = lnkDAL;
    }

    @Override
    public LinkDetail getLinkDetail(Long linkId) {

        return  _lnkDAL.getLinkDetail(linkId);
    }

    @Override
    public LinkDetail addLinkDetail(LinkDetail linkDetail) {

        return _lnkDAL.addLinkDetail(linkDetail);
    }

    @Override
    public boolean isVerifiable(Long linkId)
    {
        return _lnkDAL.isVerifiable(linkId);
    }

    @Override
    public LinkDetail SearchVerifiedLinkId(Long linkid) {
        LinkDetail ld = getLinkDetail(linkid);
        if(ld != null){
            if(!ld.isVerified()){
                ld = null;
            }
        }
        return ld;
    }

    @Override
    public LinkDetail SearchLinkId(Long linkid) {
        return getLinkDetail(linkid);
    }

    @Override
    public boolean deleteLinkDetail(Long linkId) {

        return _lnkDAL.deleteLinkDetail(linkId);
    }

    @Override
    public LinkDetail updateLinkDetail(LinkDetail document) {

        return _lnkDAL.updateLinkDetail(document);
    }
}
