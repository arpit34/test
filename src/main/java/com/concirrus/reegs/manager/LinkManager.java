package com.concirrus.reegs.manager;

import com.concirrus.reegs.model.VerifySpeedLimit.LinkDetail;


public interface LinkManager {

    LinkDetail getLinkDetail(Long linkId);

    LinkDetail addLinkDetail(LinkDetail document);

    boolean deleteLinkDetail(Long linkId);

    LinkDetail updateLinkDetail(LinkDetail document);

    boolean isVerifiable(Long linkId);

    LinkDetail SearchVerifiedLinkId(Long linkid);

    LinkDetail SearchLinkId(Long linkid);


}
