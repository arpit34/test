package com.concirrus.reegs.manager;

import com.concirrus.reegs.model.RoadSpeedRequest.Journey;
import com.concirrus.reegs.model.RoadSpeedResponse.RMEEvent;
import java.io.IOException;
import java.util.List;

public interface RMEEventManager {

    List<RMEEvent> getRMEEvents(List<Journey> listOfJourney) throws IOException;
    List<RMEEvent> getReEvaluateRMEEvents(List<RMEEvent> listOfEvent) throws IOException;

}
