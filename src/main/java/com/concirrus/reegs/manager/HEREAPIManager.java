package com.concirrus.reegs.manager;

import com.concirrus.reegs.model.RoadSpeedRequest.Location;
import java.io.IOException;

public interface HEREAPIManager {
    String getGPXXML(Location[] latlngPointsFromJourney);
    String getLinkDataFromHere(String gpxXML) throws IOException;
}
