package com.concirrus.reegs.model.enums;

public class ServiceResponse
{
    public com.concirrus.reegs.model.enums.ResponseCode ResponseCode;

    public String ErrorMessage;

    public Object ResponseData;

}