package com.concirrus.reegs.model.RoadSpeedResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RMEWarning {
    @JsonIgnore
    @JsonProperty("category")
    private String category;

    @JsonIgnore
    @JsonProperty("text")
    private String text;

    @JsonIgnore
    @JsonProperty("routeLinkSeqNum")
    private double routeLinkSeqNum;

    @JsonIgnore
    @JsonProperty("tracePointSeqNum")
    private double tracePointSeqNum;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getRouteLinkSeqNum() {
        return routeLinkSeqNum;
    }

    public void setRouteLinkSeqNum(double routeLinkSeqNum) {
        this.routeLinkSeqNum = routeLinkSeqNum;
    }

    public double getTracePointSeqNum() {
        return tracePointSeqNum;
    }

    public void setTracePointSeqNum(double tracePointSeqNum) {
        this.tracePointSeqNum = tracePointSeqNum;
    }
}
