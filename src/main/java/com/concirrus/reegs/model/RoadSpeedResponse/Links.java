package com.concirrus.reegs.model.RoadSpeedResponse;

/**
 * @author : Arpit Jain
 * @since : 15/09/17
 */

public class Links {
    private  Long linkid;

    private  String  shape;

    private  double  speedLimitKPH;

    private  String  source;

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Long getLinkid() {
        return linkid;
    }

    public void setLinkid(Long linkid) {
        this.linkid = linkid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public double getSpeedLimitKPH() {
        return speedLimitKPH;
    }

    public void setSpeedLimitKPH(double speedLimitKPH) {
        this.speedLimitKPH = speedLimitKPH;
    }
}
