package com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.Attributes;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LINK_ATTRIBUTE_FCN {
    @JsonProperty("RAMP")
    private  String RAMP;

    public String getRAMP() {
        return RAMP;
    }

    public void setRAMP(String RAMP) {
        this.RAMP = RAMP;
    }
}
