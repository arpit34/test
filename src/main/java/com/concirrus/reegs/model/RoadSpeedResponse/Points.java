package com.concirrus.reegs.model.RoadSpeedResponse;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class Points {

    private double latitude;

    private double longitude;

    private double speedLimitKPH;

    private Long LinkID;

    private double DriverSpeedKPH;

    private double Course;

    @JsonProperty("EventDateTime")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date eventDateTime;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Long getLinkID() {
        return LinkID;
    }

    public void setLinkID(Long linkID) {
        LinkID = linkID;
    }

    public Date getEventDateTime() {
        return eventDateTime;
    }

    public void setEventDateTime(Date eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    public double getCourse() {
        return Course;
    }

    public void setCourse(double course) {
        Course = course;
    }

    public double getSpeedLimitKPH() {
        return speedLimitKPH;
    }

    public void setSpeedLimitKPH(double speedLimitKPH) {
        this.speedLimitKPH = speedLimitKPH;
    }

    public double getDriverSpeedKPH() {
        return DriverSpeedKPH;
    }

    public double setDriverSpeedKPH(double driverSpeedKPH) {
        DriverSpeedKPH = driverSpeedKPH;
        return driverSpeedKPH;
    }
}
