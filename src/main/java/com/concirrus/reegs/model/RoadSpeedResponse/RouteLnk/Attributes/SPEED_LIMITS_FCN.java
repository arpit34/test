package com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.Attributes;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SPEED_LIMITS_FCN {

    @JsonProperty("FROM_REF_SPEED_LIMIT")
    private double FROM_REF_SPEED_LIMIT;

    @JsonProperty("TO_REF_SPEED_LIMIT")
    private double TO_REF_SPEED_LIMIT;

    public double getFROM_REF_SPEED_LIMIT() {
        return FROM_REF_SPEED_LIMIT;
    }

    public void setFROM_REF_SPEED_LIMIT(double FROM_REF_SPEED_LIMIT) {
        this.FROM_REF_SPEED_LIMIT = FROM_REF_SPEED_LIMIT;
    }

    public double getTO_REF_SPEED_LIMIT() {
        return TO_REF_SPEED_LIMIT;
    }

    public void setTO_REF_SPEED_LIMIT(double TO_REF_SPEED_LIMIT) {
        this.TO_REF_SPEED_LIMIT = TO_REF_SPEED_LIMIT;
    }
}
