package com.concirrus.reegs.model.RoadSpeedResponse;

import jdk.nashorn.internal.ir.annotations.Ignore;

import java.util.Date;

public class TracePoint {

    private Integer confidenceValue;
    private Integer elevation;
    private Integer headingDegreeNorthClockwise;
    private Integer headingMatched;
    private Double  lat;
    private Double  lon;
    private Double  latMatched;
    private Long    linkIdMatched;
    private Double  lonMatched;
    private Double  matchDistance;
    private Double  matchOffsetOnLink;
    private Integer minError;
    private Integer routeLinkSeqNrMatched;
    private double  speedMps;
    private Date    timestamp;

    public Integer getConfidenceValue() {
        return confidenceValue;
    }

    public void setConfidenceValue(Integer confidenceValue) {
        this.confidenceValue = confidenceValue;
    }

    public Integer getElevation() {
        return elevation;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public Integer getHeadingDegreeNorthClockwise() {
        return headingDegreeNorthClockwise;
    }

    public void setHeadingDegreeNorthClockwise(Integer headingDegreeNorthClockwise) {
        this.headingDegreeNorthClockwise = headingDegreeNorthClockwise;
    }

    public Integer getHeadingMatched() {
        return headingMatched;
    }

    public void setHeadingMatched(Integer headingMatched) {
        this.headingMatched = headingMatched;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLatMatched() {
        return latMatched;
    }

    public void setLatMatched(Double latMatched) {
        this.latMatched = latMatched;
    }

    public Long getLinkIdMatched() {
        return linkIdMatched;
    }

    public void setLinkIdMatched(Long linkIdMatched) {
        this.linkIdMatched = linkIdMatched;
    }

    public Double getLonMatched() {
        return lonMatched;
    }

    public void setLonMatched(Double lonMatched) {
        this.lonMatched = lonMatched;
    }

    public Double getMatchDistance() {
        return matchDistance;
    }

    public void setMatchDistance(Double matchDistance) {
        this.matchDistance = matchDistance;
    }

    public Double getMatchOffsetOnLink() {
        return matchOffsetOnLink;
    }

    public void setMatchOffsetOnLink(Double matchOffsetOnLink) {
        this.matchOffsetOnLink = matchOffsetOnLink;
    }

    public Integer getMinError() {
        return minError;
    }

    public void setMinError(Integer minError) {
        this.minError = minError;
    }

    public Integer getRouteLinkSeqNrMatched() {
        return routeLinkSeqNrMatched;
    }

    public void setRouteLinkSeqNrMatched(Integer routeLinkSeqNrMatched) {
        this.routeLinkSeqNrMatched = routeLinkSeqNrMatched;
    }

    public double getSpeedMps() {
        return speedMps;
    }

    public void setSpeedMps(double speedMps) {
        this.speedMps = speedMps;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
