package com.concirrus.reegs.model.RoadSpeedResponse;

import com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.RouteLink;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;


public class RMEResponse {
    @JsonProperty("RouteLinks")
    private List<RouteLink> RouteLinks;

    @JsonProperty("TracePoints")
    private List<TracePoint> TracePoints;

    @JsonProperty("Warnings")
    private List<RMEWarning> Warnings;

    @JsonProperty("MapVersion")
    private String MapVersion;

    public List<RouteLink> getRouteLinks() {
        return RouteLinks;
    }

    public void setRouteLinks(List<RouteLink> routeLinks) {
        RouteLinks = routeLinks;
    }

    public List<TracePoint> getTracePoints() {
        return TracePoints;
    }

    public void setTracePoints(List<TracePoint> tracePoints) {
        TracePoints = tracePoints;
    }

    public List<RMEWarning> getWarnings() {
        return Warnings;
    }

    public void setWarnings(List<RMEWarning> warnings) {
        Warnings = warnings;
    }

    public String getMapVersion() {
        return MapVersion;
    }

    public void setMapVersion(String mapVersion) {
        MapVersion = mapVersion;
    }


}
