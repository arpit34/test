package com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk;

import com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.Attributes.LINK_ATTRIBUTE_FCN;
import com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.Attributes.SPEED_LIMITS_FCN;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author : Arpit Jain
 * @since : 15/09/17
 */

public class Attribute {

    @JsonProperty("LINK_ATTRIBUTE_FCN")
    private  List<LINK_ATTRIBUTE_FCN> LINK_ATTRIBUTE_FCN;

    @JsonProperty("SPEED_LIMITS_FCN")
    private List<SPEED_LIMITS_FCN> SPEED_LIMITS_FCN;

    public List<com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.Attributes.LINK_ATTRIBUTE_FCN> getLINK_ATTRIBUTE_FCN() {
        return LINK_ATTRIBUTE_FCN;
    }

    public void setLINK_ATTRIBUTE_FCN(List<com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.Attributes.LINK_ATTRIBUTE_FCN> LINK_ATTRIBUTE_FCN) {
        this.LINK_ATTRIBUTE_FCN = LINK_ATTRIBUTE_FCN;
    }

    public List<com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.Attributes.SPEED_LIMITS_FCN> getSPEED_LIMITS_FCN() {
        return SPEED_LIMITS_FCN;
    }

    public void setSPEED_LIMITS_FCN(List<com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk.Attributes.SPEED_LIMITS_FCN> SPEED_LIMITS_FCN) {
        this.SPEED_LIMITS_FCN = SPEED_LIMITS_FCN;
    }
}
