package com.concirrus.reegs.model.RoadSpeedResponse.RouteLnk;

import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;

/**
 * @author : Arpit Jain
 * @since : 15/09/17
 */

public class RouteLink {

    private Long linkId;

    private Integer functionalClass;

    private Integer confidence;

    private Double  linkLength;

    private Integer mSecToReachLinkFromStart;

    private String  shape;

    @JsonProperty("attributes")
    private Attribute attributes;

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public Integer getFunctionalClass() {
        return functionalClass;
    }

    public void setFunctionalClass(Integer functionalClass) {
        this.functionalClass = functionalClass;
    }

    public Integer getConfidence() {
        return confidence;
    }

    public void setConfidence(Integer confidence) {
        this.confidence = confidence;
    }

    public Double getLinkLength() {
        return linkLength;
    }

    public void setLinkLength(Double linkLength) {
        this.linkLength = linkLength;
    }

    public Integer getmSecToReachLinkFromStart() {
        return mSecToReachLinkFromStart;
    }

    public void setmSecToReachLinkFromStart(Integer mSecToReachLinkFromStart) {
        this.mSecToReachLinkFromStart = mSecToReachLinkFromStart;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Attribute getAttributes() {
        return attributes;
    }

    public void setAttributes(Attribute attributes) {
        this.attributes = attributes;
    }
}
