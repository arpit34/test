package com.concirrus.reegs.model.RoadSpeedResponse;

import com.concirrus.reegs.model.enums.RMEEventType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;
import java.util.List;

@JsonPropertyOrder({ "isExtreme", "verified","imei","datetime","roadLimitAtPeakKPH","eventDistanceMetre","eventDurationSeconds","driverSpeedAtPeakKPH","eventAverageDriverSpeedKPH", "links","points"})

public class RMEEvent {

    @JsonIgnore
    private Long linkId;
    @JsonIgnore
    private boolean reviewRequired;
    @JsonIgnore
    private Double lat;
    @JsonIgnore
    private Double lon;
    @JsonIgnore
    private String startpoint;
    @JsonIgnore
    private String endpoint;
    @JsonIgnore
    private RMEEventType eventType;
    @JsonIgnore
    private boolean ignored;
    @JsonIgnore
    private boolean verifiable;
    @JsonIgnore
    private double SpeedLimit;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date datetime;

    private Double  DriverSpeedAtPeakKPH;
    private Double  RoadLimitAtPeakKPH;
    private double  EventDurationSeconds;
    private double  EventAverageDriverSpeedKPH;
    private double  EventDistanceMetre;
    private boolean isExtreme;
    private Long  IMEI;
    private boolean Verified;
    private List<Links> Links;
    private List<Points> Points;

    public boolean isReviewRequired() {
        return reviewRequired;
    }

    public void setReviewRequired(boolean reviewRequired) {
        this.reviewRequired = reviewRequired;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public RMEEventType getEventType() {
        return eventType;
    }

    public void setEventType(RMEEventType eventType) {
        this.eventType = eventType;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public boolean isIgnored() {
        return ignored;
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }

    public boolean isverifiable() {
        return verifiable;
    }

    public void setverifiable(boolean verifiable) {
        this.verifiable = verifiable;
    }

    public boolean getIsExtreme() {
        return isExtreme;
    }

    public void setIsExtreme(boolean isExtreme) {
        this.isExtreme = isExtreme;
    }

    public boolean isVerified() {
        return Verified;
    }

    public void setVerified(boolean verified) {
        Verified = verified;
    }

    public String getStartpoint() {
        return startpoint;
    }

    public void setStartpoint(String startpoint) {
        this.startpoint = startpoint;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public List<com.concirrus.reegs.model.RoadSpeedResponse.Links> getLinks() {
        return Links;
    }

    public void setLinks(List<com.concirrus.reegs.model.RoadSpeedResponse.Links> links) {
        Links = links;
    }

    public List<com.concirrus.reegs.model.RoadSpeedResponse.Points> getPoints() {
        return Points;
    }

    public void setPoints(List<com.concirrus.reegs.model.RoadSpeedResponse.Points> points) {
        Points = points;
    }

    public Long getIMEI() {
        return IMEI;
    }

    public void setIMEI(Long IMEI) {
        this.IMEI = IMEI;
    }

    public double getEventDurationSeconds() {
        return EventDurationSeconds;
    }

    public void setEventDurationSeconds(double eventDurationSeconds) {
        EventDurationSeconds = eventDurationSeconds;
    }

    public double getEventDistanceMetre() {
        return EventDistanceMetre;
    }

    public void setEventDistanceMetre(double eventDistanceMetre) {
        EventDistanceMetre = eventDistanceMetre;
    }

    public Double getDriverSpeedAtPeakKPH() {
        return DriverSpeedAtPeakKPH;
    }

    public void setDriverSpeedAtPeakKPH(Double driverSpeedAtPeakKPH) {
        DriverSpeedAtPeakKPH = driverSpeedAtPeakKPH;
    }

    public Double getRoadLimitAtPeakKPH() {
        return RoadLimitAtPeakKPH;
    }

    public void setRoadLimitAtPeakKPH(Double roadLimitAtPeakKPH) {
        RoadLimitAtPeakKPH = roadLimitAtPeakKPH;
    }

    public double getEventAverageDriverSpeedKPH() {
        return EventAverageDriverSpeedKPH;
    }

    public void setEventAverageDriverSpeedKPH(double eventAverageDriverSpeedKPH) {
        EventAverageDriverSpeedKPH = eventAverageDriverSpeedKPH;
    }

    public double getSpeedLimit() {
        return SpeedLimit;
    }

    @JsonIgnore
    public void setSpeedLimit(double speedLimit) {
        SpeedLimit = speedLimit;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }
}

