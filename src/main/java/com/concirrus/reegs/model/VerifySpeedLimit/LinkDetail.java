package com.concirrus.reegs.model.VerifySpeedLimit;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "LinkDetails")
public class LinkDetail {

    @Id
    private String id;

    @Indexed(unique = true)

    private Long linkId;
    private double verifiedSpeedLimit;
    private String verifiedBy = "";
    private boolean verifiable;
    private boolean verified;
    private String comments = "";

    @JsonProperty("lastUpdatedOn")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date lastUpdatedOn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public double getVerifiedSpeedLimit() {
        return verifiedSpeedLimit;
    }

    public void setVerifiedSpeedLimit(double speedLimit) {
        this.verifiedSpeedLimit = speedLimit;
    }

    public Date getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date reviewLastUpdatedOn) {
        this.lastUpdatedOn = reviewLastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public boolean isVerifiable() {
        return verifiable;
    }

    public void setVerifiable(boolean verifiable) {
        this.verifiable = verifiable;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
