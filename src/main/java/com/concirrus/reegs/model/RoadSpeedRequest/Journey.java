package com.concirrus.reegs.model.RoadSpeedRequest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Journey {

    @JsonProperty("Location")
    private Location[] location;

    @JsonProperty("IMEI")
    private Long IMEI;

    public Location[] getLocation() {
        return location;
    }

    public void setLocation(Location[] location) {
        this.location = location;
    }

    public Long getIMEI() {
        return IMEI;
    }

    public void setIMEI(Long IMEI) {
        this.IMEI = IMEI;
    }
}
