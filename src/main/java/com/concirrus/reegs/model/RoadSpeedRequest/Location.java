package com.concirrus.reegs.model.RoadSpeedRequest;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;


public class Location {

    @JsonProperty("Longitude")
    private double longitude;

    @JsonProperty("Latitude")
    private double latitude;

    @JsonProperty("DriverSpeedKPH")
    private Double DriverSpeedKPH;

    @JsonProperty("Course")
    private double Course;

    @JsonProperty("EventDateTime")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date eventDateTime;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Date getEventDateTime() {
        return eventDateTime;
    }

    public void setEventDateTime(Date eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    public Double getDriverSpeedKPH() {
        return DriverSpeedKPH;
    }

    public void setDriverSpeedKPH(Double driverSpeedKPH) {
        DriverSpeedKPH = driverSpeedKPH;
    }

    public double getCourse() {
        return Course;
    }

    public void setCourse(double course) {
        Course = course;
    }
}
