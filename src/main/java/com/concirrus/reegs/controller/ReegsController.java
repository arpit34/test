package com.concirrus.reegs.controller;

import com.concirrus.reegs.model.RoadSpeedRequest.Location;
import com.concirrus.reegs.model.RoadSpeedResponse.Points;
import com.concirrus.reegs.model.enums.ResponseCode;
import com.concirrus.reegs.model.enums.ServiceResponse;
import com.concirrus.reegs.manager.LinkManager;
import com.concirrus.reegs.manager.RMEEventManager;
import com.concirrus.reegs.model.RoadSpeedRequest.Journey;
import com.concirrus.reegs.model.VerifySpeedLimit.LinkDetail;
import com.concirrus.reegs.model.RoadSpeedResponse.RMEEvent;
import com.mongodb.DuplicateKeyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/reegs")
public class ReegsController {
    private final Logger _logger = LoggerFactory.getLogger(ReegsController.class);

    private RMEEventManager _rmeEventManager;
    private LinkManager _linkManager;

    @Autowired
    public ReegsController(RMEEventManager rmeEventManager, LinkManager linkManager) {
        this._rmeEventManager = rmeEventManager;
        this._linkManager = linkManager;
    }

    /**
     * @return List of RMEEvent
     * @throws
     * @description This method will give rme events if availabe in a journey
     */
    @RequestMapping(value = "/roadspeedeevents", method = RequestMethod.POST)
    public List<RMEEvent> getRMEEvents(@RequestBody List<Journey> listOfJourney) throws IOException {
        return _rmeEventManager.getRMEEvents(listOfJourney);
    }

    /**
     * @return List of reevalute
     * @throws
     */
    @RequestMapping(value = "/event/reevaluate", method = RequestMethod.POST)
    public List<RMEEvent> getReEvaluateRMEEvents(@RequestBody List<RMEEvent> listOfEvent) throws IOException {
        List<RMEEvent> events = _rmeEventManager.getReEvaluateRMEEvents(listOfEvent);
        return events;
    }

    /**
     * @return Link Details
     * @throws
     * @description This method gets manual Document result from mongoDB
     */
    @RequestMapping(value = "/verifiedSpeedLimits/{linkId}", method = RequestMethod.GET)
    public ServiceResponse getLinkDetail(@PathVariable Long linkId) {

        ServiceResponse serviceResponse = new ServiceResponse();
        try {
            LinkDetail link = _linkManager.getLinkDetail(linkId);

            if (link != null) {
                serviceResponse.ResponseCode = ResponseCode.Successful;
                serviceResponse.ResponseData = link;
                serviceResponse.ErrorMessage = null;
            } else {
                serviceResponse.ResponseCode = ResponseCode.Failed;
                serviceResponse.ErrorMessage = "No link details found for linkId '"+linkId+"'.";
                serviceResponse.ResponseData = null;
            }
        } catch (Exception ex) {
            _logger.error("Exception occured while searching linkId. More details: '{}'", ex.getMessage());
            serviceResponse.ResponseCode = ResponseCode.Failed;
            serviceResponse.ErrorMessage = "Exception occured. More details: '" + ex.getMessage() + "'";
            serviceResponse.ResponseData = null;
        }
        return serviceResponse;
    }

    /**
     * @return Link details.
     * @throws 'Duplicate key exception'
     * @description First of all this method checks in mongoDB for the existing link and if link already exists
     * then it returns a message with link details if not then inserts new link details  into mongoDB
     */
    @RequestMapping(value = "/verifiedSpeedLimits", method = RequestMethod.POST)
    public ServiceResponse addLinkDetail(@RequestBody LinkDetail linkDetail) {

        ServiceResponse serviceResponse = new ServiceResponse();
        try {
            Long linkId = linkDetail.getLinkId();
            LinkDetail link = _linkManager.getLinkDetail(linkId);

            if (link == null) {
                link = _linkManager.addLinkDetail(linkDetail);

                if (link != null) {
                    serviceResponse.ResponseCode = ResponseCode.Successful;
                    serviceResponse.ResponseData = link;
                    serviceResponse.ErrorMessage = null;
                } else {
                    serviceResponse.ResponseCode = ResponseCode.Failed;
                    serviceResponse.ResponseData = null;
                    serviceResponse.ErrorMessage = "Link not added.";
                }
            } else {
                serviceResponse.ResponseCode = ResponseCode.Failed;
                serviceResponse.ErrorMessage = "You are trying to insert duplicate linkId";
                serviceResponse.ResponseData = link;
            }
        } catch (DuplicateKeyException ex) {
            serviceResponse.ResponseCode = ResponseCode.Failed;
            serviceResponse.ErrorMessage = "You are trying to insert duplicate link.";
            serviceResponse.ResponseData = null;
        } catch (Exception ex) {
            _logger.error("Exception occured while inserting link id. '{}'", ex.getMessage());
            serviceResponse.ResponseCode = ResponseCode.Failed;
            serviceResponse.ErrorMessage = "Exception encountered. Details: '" + ex.getMessage() + "'";
            serviceResponse.ResponseData = null;
        }

        return serviceResponse;
    }

    /**
     * @return Delete Link details.
     * @throws
     * @description First of all this method checks in mongoDB for the existing link and if link exists
     * then it deletes the link details from mongoDB.
     */
    @RequestMapping(value = "/verifiedSpeedLimits/{linkId}", method = RequestMethod.DELETE)
    public ServiceResponse deleteLinkDetail(@PathVariable Long linkId) {

        ServiceResponse serviceResponse = new ServiceResponse();
        try {
            LinkDetail link = _linkManager.getLinkDetail(linkId);

            if (link != null) {
                boolean status = _linkManager.deleteLinkDetail(linkId);

                if (status) {
                    serviceResponse.ResponseCode = ResponseCode.Successful;
                    serviceResponse.ResponseData = null;
                    serviceResponse.ErrorMessage = null;
                } else {
                    serviceResponse.ResponseCode = ResponseCode.Failed;
                    serviceResponse.ErrorMessage = "Link could not be deleted.";
                    serviceResponse.ResponseData = null;
                }
            } else {
                serviceResponse.ResponseCode = ResponseCode.Failed;
                serviceResponse.ResponseData = null;
                serviceResponse.ErrorMessage = "Link '" + linkId + "' does not exists in the database.";
            }
        } catch (Exception ex) {
            _logger.error("Exception occured while deleting link: '" + linkId + "'." +
                    "More details: '" + ex.getMessage() + "'");
            serviceResponse.ResponseCode = ResponseCode.Failed;
            serviceResponse.ResponseData = null;
            serviceResponse.ErrorMessage = "An exception has been encountered. Details: '" + ex.getMessage() + "'";
        }
        return serviceResponse;
    }

    /**
     * @return Update Link details.
     * @throws
     * @description First of all this method checks in mongoDB for the existing link and if link already exists
     * then it returns a message with ResponseCode as 'Failed' and then link details, if not exists then updates
     * link details provided by user into mongoDB
     */
    @RequestMapping(value = "/verifiedSpeedLimits", method = RequestMethod.PUT)
    public ServiceResponse updateLinkDetail(@RequestBody LinkDetail linkDetail) {

        ServiceResponse serviceResponse = new ServiceResponse();
        Long linkId = linkDetail.getLinkId();

        try {
            LinkDetail link = _linkManager.getLinkDetail(linkId);
            if (link != null) {
                link = _linkManager.updateLinkDetail(linkDetail);

                if (link != null) {
                    serviceResponse.ResponseCode = ResponseCode.Successful;
                    serviceResponse.ResponseData = link;
                    serviceResponse.ErrorMessage = null;
                } else {
                    serviceResponse.ResponseCode = ResponseCode.Failed;
                    serviceResponse.ErrorMessage = "Record not updated.";
                    serviceResponse.ResponseData = null;
                }
            } else {
                serviceResponse.ResponseCode = ResponseCode.Failed;
                serviceResponse.ResponseData = null;
                serviceResponse.ErrorMessage = "No link found to update.";
            }
        } catch (Exception ex) {
            _logger.error("Exception occured while updating link: '" + linkId + "'." +
                    "More details: '" + ex.getMessage() + "'");
            serviceResponse.ResponseCode = ResponseCode.Failed;
            serviceResponse.ResponseData = null;
            serviceResponse.ErrorMessage = "An exception has been encountered. Details: '" + ex.getMessage() + "'";
        }

        return serviceResponse;
    }
}
