package com.concirrus.reegs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author : Arpit Jain
 * @since : 15/09/17
 */

@EnableCaching
@SpringBootApplication
public class ReegsApp {

    public static void main( String[] args )
    {
        SpringApplication.run(ReegsApp.class, args);
    }
}
